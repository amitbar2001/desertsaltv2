const script = document.createElement("script");
script.setAttribute("type", "module");
script.setAttribute("src", chrome.runtime.getURL("NoderNeder.js"));
const head = document.head || document.getElementsByTagName("head")[0] || document.documentElement;
head.insertBefore(script, head.lastChild);

// =============================
//     [Non relevant code]
// Forced to use chrome.runtime
// Without external modules
// =============================

const link = document.createElement("link");
link.rel = "stylesheet";
link.href = chrome.runtime.getURL("notification.css");
head.insertBefore(link, head.lastChild);

// ====================
// options
// ====================

const positions = {
  top: "top",
  bottom: "bottom",
};

let options = {
  alertTime: 3.5,
  overlayClickDismiss: true,
  overlayOpacity: 0.75,
  transitionCurve: "ease",
  transitionDuration: 0.3,
  transitionSelector: "all",
  classes: {
    container: "notie-container",
    textbox: "notie-textbox",
    textboxInner: "notie-textbox-inner",
    button: "notie-button",
    element: "notie-element",
    elementHalf: "notie-element-half",
    elementThird: "notie-element-third",
    overlay: "notie-overlay",
    backgroundSuccess: "notie-background-success",
    backgroundWarning: "notie-background-warning",
    backgroundError: "notie-background-error",
    backgroundInfo: "notie-background-info",
    backgroundNeutral: "notie-background-neutral",
    backgroundOverlay: "notie-background-overlay",
    alert: "notie-alert",
    inputField: "notie-input-field",
    selectChoiceRepeated: "notie-select-choice-repeated",
  },
  ids: {
    overlay: "notie-overlay",
  },
  positions: {
    alert: positions.bottom,
    confirm: positions.bottom,
  },
};

const setOptions = (newOptions) => {
  options = {
    ...options,
    ...newOptions,
    classes: { ...options.classes, ...newOptions.classes },
    ids: { ...options.ids, ...newOptions.ids },
    positions: { ...options.positions, ...newOptions.positions },
  };
};

// ====================
// helpers
// ====================

const tick = () => new Promise((resolve) => setTimeout(resolve, 0));
const wait = (time) => new Promise((resolve) => setTimeout(resolve, time * 1000));

const blur = () => {
  document.activeElement && document.activeElement.blur();
};

const generateRandomId = () => {
  // RFC4122 version 4 compliant UUID
  const id = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    const r = (Math.random() * 16) | 0;
    const v = c === "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
  return `notie-${id}`;
};

const typeToClassLookup = {
  1: options.classes.backgroundSuccess,
  success: options.classes.backgroundSuccess,
  2: options.classes.backgroundWarning,
  warning: options.classes.backgroundWarning,
  3: options.classes.backgroundError,
  error: options.classes.backgroundError,
  4: options.classes.backgroundInfo,
  info: options.classes.backgroundInfo,
  5: options.classes.backgroundNeutral,
  neutral: options.classes.backgroundNeutral,
};

const getTransition = () =>
  `${options.transitionSelector} ${options.transitionDuration}s ${options.transitionCurve}`;

const enterClicked = (event) => event.keyCode === 13;
const escapeClicked = (event) => event.keyCode === 27;

const addToDocument = (element, position) => {
  element.classList.add(options.classes.container);
  element.style[position] = "-10000px";
  document.body.appendChild(element);
  element.style[position] = `-${element.offsetHeight}px`;

  if (element.listener) window.addEventListener("keydown", element.listener);

  tick().then(() => {
    element.style.transition = getTransition();
    element.style[position] = 0;
  });
};

const removeFromDocument = (id, position) => {
  const element = document.getElementById(id);
  if (!element) return;
  element.style[position] = `-${element.offsetHeight}px`;

  if (element.listener) window.removeEventListener("keydown", element.listener);

  wait(options.transitionDuration).then(() => {
    if (element.parentNode) element.parentNode.removeChild(element);
  });
};

const addOverlayToDocument = (owner, position) => {
  const element = document.createElement("div");
  element.id = options.ids.overlay;
  element.classList.add(options.classes.overlay);
  element.classList.add(options.classes.backgroundOverlay);
  element.style.opacity = 0;
  if (owner && options.overlayClickDismiss) {
    element.onclick = () => {
      removeFromDocument(owner.id, position);
      removeOverlayFromDocument();
    };
  }

  document.body.appendChild(element);

  tick().then(() => {
    element.style.transition = getTransition();
    element.style.opacity = options.overlayOpacity;
  });
};

const removeOverlayFromDocument = () => {
  const element = document.getElementById(options.ids.overlay);
  element.style.opacity = 0;
  wait(options.transitionDuration).then(() => {
    if (element.parentNode) element.parentNode.removeChild(element);
  });
};

const hideAlerts = (callback) => {
  const alertsShowing = document.getElementsByClassName(options.classes.alert);
  if (alertsShowing.length) {
    for (let i = 0; i < alertsShowing.length; i++) {
      const alert = alertsShowing[i];
      removeFromDocument(alert.id, alert.position);
    }
    if (callback) wait(options.transitionDuration).then(() => callback());
  }
};

// ====================
// exports
// ====================

const alertThis = ({
  type = 1,
  text,
  time = options.alertTime,
  stay = false,
  position = options.positions.alert || position.top,
}) => {
  blur();
  hideAlerts();

  const element = document.createElement("div");
  const id = generateRandomId();
  element.id = id;
  element.position = position;
  element.classList.add(options.classes.textbox);
  element.classList.add(typeToClassLookup[type]);
  element.classList.add(options.classes.alert);
  element.innerHTML = `<div class="${options.classes.textboxInner}">${text}</div>`;
  element.onclick = () => removeFromDocument(id, position);

  element.listener = (event) => {
    if (enterClicked(event) || escapeClicked(event)) hideAlerts();
  };

  addToDocument(element, position);

  if (time && time < 1) time = 1;
  if (!stay && time) wait(time).then(() => removeFromDocument(id, position));
};

const confirm = (
  {
    text,
    submitText = "Yes",
    cancelText = "Cancel",
    submitCallback,
    cancelCallback,
    position = options.positions.confirm || position.top,
  },
  submitCallbackArg,
  cancelCallbackArg
) => {
  blur();
  hideAlerts();

  const element = document.createElement("div");
  const id = generateRandomId();
  element.id = id;

  const elementText = document.createElement("div");
  elementText.classList.add(options.classes.textbox);
  elementText.classList.add(options.classes.backgroundInfo);
  elementText.innerHTML = `<div class="${options.classes.textboxInner}">${text}</div>`;

  const elementButtonLeft = document.createElement("div");
  elementButtonLeft.classList.add(options.classes.button);
  elementButtonLeft.classList.add(options.classes.elementHalf);
  elementButtonLeft.classList.add(options.classes.backgroundSuccess);
  elementButtonLeft.innerHTML = submitText;
  elementButtonLeft.onclick = () => {
    removeFromDocument(id, position);
    removeOverlayFromDocument();
    if (submitCallback) submitCallback();
    else if (submitCallbackArg) submitCallbackArg();
  };

  const elementButtonRight = document.createElement("div");
  elementButtonRight.classList.add(options.classes.button);
  elementButtonRight.classList.add(options.classes.elementHalf);
  elementButtonRight.classList.add(options.classes.backgroundError);
  elementButtonRight.innerHTML = cancelText;
  elementButtonRight.onclick = () => {
    removeFromDocument(id, position);
    removeOverlayFromDocument();
    if (cancelCallback) cancelCallback();
    else if (cancelCallbackArg) cancelCallbackArg();
  };

  element.appendChild(elementText);
  element.appendChild(elementButtonLeft);
  element.appendChild(elementButtonRight);

  element.listener = (event) => {
    if (enterClicked(event)) elementButtonLeft.click();
    else if (escapeClicked(event)) elementButtonRight.click();
  };

  addToDocument(element, position);

  addOverlayToDocument(element, position);
};

chrome.runtime.onMessage.addListener(function (request, _sender, sendResponse) {
  switch (request.result) {
    case "success":
      sendResponse({ farewell: "goodbye" });
      alertThis({ text: "נייס! ההצעה נשלחה לעמית עיליי בר" });

      break;
    case "it's fucked":
      sendResponse({ farewell: "goodbye" });
      alertThis({ text: "איייייי משהו לא עבד, נסו שוב מאוחר יותר", type: 3 });

      break;
    case "salt":
      sendResponse({ isAllowed: window.localStorage.getItem("isEilay") });

      break;
    case "words":
      sendResponse({ farewell: "changed" });
      alertThis({ text: `Editing words dictionairy` });
      window.localStorage.setItem("shouldReloadDB", true);

      break;
    case "names":
      sendResponse({ farewell: "names" });
      alertThis({ text: `Editing names dictionairy` });

      break;
    case "images":
      sendResponse({ farewell: "changed" });
      alertThis({ text: `Editing images dictionairy` });

      break;

    case "reloadDB":
      sendResponse({ farewell: "reload" });
      window.localStorage.setItem("shouldReloadDB", true);

      break;
    default:
      break;
  }
});
