// Content.js code

import { confetti, killConfetti } from "./confetti.js";
import {
  updateMessages,
  updateChats,
  updateCard,
  reloadDB,
} from "./updaters.js";

const timeItTakesForTeamsToLoad = 12000;
const confettiPrepend = 3000;
const loopInterval = 4000;
let messagesUpdateAllowed = true;
let chatsUpdateAllowed = true;
let cardUpdateAllowed = true;
let messagesUpdateNeeded = true;
let chatsUpdateNeeded = true;
let cardUpdateNeeded = true;

window.onload = function () {
  setTimeout(
    () => confetti("page-content-wrapper", 500),
    timeItTakesForTeamsToLoad - confettiPrepend
  );
  setTimeout(killConfetti, timeItTakesForTeamsToLoad - 1030);
  setTimeout(setup, timeItTakesForTeamsToLoad);
};

const setup = () => {
  setupObservers();
  mainLoop();
};

const setupObservers = () => {
  observePage();
  observeMessages();
  observeChats();
  observeCard();
};

const mainLoop = async () => {
  messagesUpdateAllowed = true;
  chatsUpdateAllowed = true;
  cardUpdateAllowed = true;

  if (dbReloadNeeded()) await dbReload();

  if (pageLoadingNeeded) pageObserver();
  if (messagesLoadingNeeded) observeMessages();
  if (chatsLoadingNeeded) observeChats();
  if (cardLoadingNeeded) observeCard();

  if (messagesUpdateNeeded && !messagesLoadingNeeded) {
    updateMessages();
    messagesUpdateAllowed = false;
    messagesUpdateNeeded = false;
  }

  if (chatsUpdateNeeded && !chatsLoadingNeeded) {
    updateChats();
    chatsUpdateAllowed = false;
    chatsUpdateNeeded = false;
  }

  if (cardUpdateNeeded && !cardLoadingNeeded) {
    updateCard();
    cardUpdateAllowed = false;
    cardUpdateNeeded = false;
  }

  setTimeout(mainLoop, loopInterval);
};

const pageChangeDetected = function pageChangeDetected() {
  setTimeout(() => {
    observeMessages();
    observeChats();
    observeCard();
  }, 100);
};

const cardChangeDetected = function cardChangeDetected() {
  if (cardUpdateAllowed) {
    setTimeout(updateCard, 1500);
  } else {
    cardUpdateNeeded = true;
  }
};

const chatsChangeDetected = function chatsChangeDetected() {
  if (chatsUpdateAllowed) {
    chatsUpdateAllowed = false;
    setTimeout(updateChats, 600);
  } else {
    chatsUpdateNeeded = true;
  }
};

let pageLoadingNeeded = false;
const observePage = () => {
  const pageTitle = document.head.getElementsByTagName("title")[0];
  const pageObserver = new MutationObserver(pageChangeDetected);

  try {
    pageObserver.observe(pageTitle, observerConfig);
  } catch (error) {
    console.log("page not loaded yet");
    pageLoadingNeeded = true;
  }
};

const messagesChangeDetected = function messagesChangeDetected() {
  if (messagesUpdateAllowed) {
    messagesUpdateAllowed = false;
    setTimeout(updateMessages, 600);
  } else {
    messagesUpdateNeeded = true;
  }
};

const messagesObserver = new MutationObserver(messagesChangeDetected);
const observerConfig = { attributes: false, childList: true, subtree: false };

let messagesLoadingNeeded = false;
const observeMessages = () => {
  let messageList = undefined;

  if (document.getElementsByClassName("message-pane")[0]) {
    messageList = document.getElementsByClassName(
      "ts-message-list-container"
    )[0];
  } else {
    const iframe = document.getElementsByClassName(
      "embedded-electron-webview embedded-page-content"
    )[0];
    const iframeDocument = iframe.contentWindow?.document;
    if (iframeDocument) {
      messageList = iframeDocument.getElementsByClassName("ui-chat")[0];
    }
  }

  try {
    messagesObserver.disconnect();
    messagesObserver.observe(messageList, observerConfig);
    messagesLoadingNeeded = false;
    messagesUpdateNeeded = true;
  } catch (error) {
    console.log("messages not loaded yet");
    messagesLoadingNeeded = true;
  }
};

const chatsObserver = new MutationObserver(chatsChangeDetected);
let chatsLoadingNeeded = false;

const observeChats = () => {
  const chats = document.body.getElementsByClassName(
    "content active ts-tree-group"
  )[0];

  try {
    chatsObserver.disconnect();
    chatsObserver.observe(chats, observerConfig);
    chatsLoadingNeeded = false;
    chatsUpdateNeeded = true;
  } catch (error) {
    if (document.body.getElementsByClassName("left-rail-header-v2")[0]) {
      console.log("no chats");
    } else {
      console.log("chat not loaded yet");
      chatsLoadingNeeded = true;
    }
  }
};

const cardObserver = new MutationObserver(cardChangeDetected);
let cardLoadingNeeded = false;

const observeCard = () => {
  const cardContainer = document.getElementById("LivePersonaCardRootElement");

  try {
    cardObserver.disconnect();
    cardObserver.observe(cardContainer, observerConfig);
    cardLoadingNeeded = false;
    cardUpdateNeeded = true;
  } catch (error) {
    console.log("card not loaded yet");
    cardLoadingNeeded = true;
  }
};

const dbReloadNeeded = function dbReloadNeeded() {
  return window.localStorage.getItem("shouldReloadDB") === "true";
};

const dbReload = async function dbReload() {
  await reloadDB();
  messagesUpdateNeeded = true;
  chatsUpdateNeeded = true;
  cardUpdateNeeded = true;
  window.localStorage.setItem("shouldReloadDB", false);
};
