const baseUrl = "https://api.github.com/repos/TopSecretSalt/Desert/contents/db.json";

const decodeBase64 = function b64DecodeUnicode(str) {
  return decodeURIComponent(
    atob(str)
      .split("")
      .map(function (c) {
        return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
      })
      .join("")
  );
};

const encodeBase64 = function b64EncodeUnicode(str) {
  return btoa(
    encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function toSolidBytes(match, p1) {
      return String.fromCharCode("0x" + p1);
    })
  );
};

const fetchDB = async function fetchDB(branch = "main", cache = true) {
  const url = cache
    ? `${baseUrl}?ref=${branch}`
    : `${baseUrl}?ref=${branch}&harta=${Math.random()}`;

  try {
    const rawResponse = await fetch(url);

    if (!rawResponse.ok) {
      const msg =
        rawResponse.status == 403
          ? "Rate limit exceeded"
          : `HTTP error! status: ${rawResponse.status}`;
      throw new Error(msg);
    }

    if (!rawResponse.ok && rawResponse.status != 302) {
      throw new Error(response.status);
    }

    const response = await rawResponse.json();
    response.content = JSON.parse(decodeBase64(response.content));
    return response;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const fetchData = async () => {
  const { content } = await fetchDB();
  return content;
};

const getSuggestions = async () => fetchDB("suggestions", false);
const getMainDB = async () => fetchDB("main", false);

const fetchDataNoCache = async () => {
  const { content } = await getMainDB();
  return content;
};

const updateDB = async function updateDB({ db, commitID, message, branch = "main" }) {
  try {
    const response = await fetch(`${baseUrl}`, {
      method: "PUT",
      headers: {
        Accept: "application/vnd.github.v3+json",
        authorization: "token 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",
      },
      body: JSON.stringify({
        message: message,
        branch: branch,
        content: encodeBase64(JSON.stringify(db, null, 2)),
        sha: commitID,
        committer: {
          name: "Amit Ahi",
          email: "fakeassemailnoder3@gmail.com",
        },
      }),
    });

    if (!response.ok && response.status != 302) {
      const msg = response.status === 409 ? "file from getSuggestions not up to date" : "";
      throw new Error(`${response.status}: ${response.statusText}  ${msg}`);
    }

    return await response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
};

const sendSuggestions = async ({ db, commitID, message }) =>
  updateDB({ db, commitID, message, branch: "suggestions" });

export { fetchData, getSuggestions, sendSuggestions, getMainDB, updateDB, fetchDataNoCache };
