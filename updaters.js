import { fetchData, fetchDataNoCache } from "./api.js";

let wordDictionary, nameDictionary, imgDictionary;
const start = async () => {
  ({
    words: wordDictionary,
    names: nameDictionary,
    images: imgDictionary,
  } = await fetchData());
};

start();

const replaceText = function replaceText(element, innerType, dictionary) {
  for (const [key, valueArr] of Object.entries(dictionary)) {
    const value =
      valueArr[
        valueArr.length === 1 ? 0 : Math.floor(Math.random() * valueArr.length)
      ];
    if (element[innerType].includes(key))
      element[innerType] = element[innerType].replaceAll(key, value);
  }
};

const replaceImg = function (element) {
  for (const [key, value] of Object.entries(imgDictionary)) {
    if (element) {
      const altOrLabel =
        element.getAttribute("aria-label") || element.getAttribute("alt");
      if (altOrLabel && altOrLabel.includes(key)) {
        element.setAttribute("src", value);
        element.setAttribute("ng-src", value);
      }
    }
  }
};

const replaceMessageContent = (msg) => {
  const content = msg.getElementsByClassName("ui-chat__messagecontent")[0];
  if (content) {
    replaceText(content, "innerText", wordDictionary);
  } else {
    replaceText(msg, "innerHTML", wordDictionary);
  }
};
const replaceName = (name) => replaceText(name, "innerText", nameDictionary);
const replaceChatContent = (chat) =>
  replaceText(chat, "innerText", wordDictionary);

const findMessageAndUpdate = function findMessageAndUpdate(text) {
  setTimeout(() => {
    const iframeDocument = document.getElementsByClassName(
      "embedded-electron-webview embedded-page-content"
    )[0].contentWindow.document;
    const messages = [
      ...iframeDocument.getElementsByClassName("ui-chat__item"),
    ];

    if (messages) {
      messages.forEach((element) => {
        if (
          (element.getElementsByClassName("ui-image").length != 0 ||
            element.getElementsByClassName("ui-grid").length != 0) &&
          element.getElementsByClassName("ui-loader").length === 0
        ) {
          if (element.innerText === text) {
            replaceMessageContent(element);
          }
        }
      });
    }
  }, 1000);
};

const findMessageAndUpdateChannel = function findMessageAndUpdateChannel(text) {
  setTimeout(() => {
    const messages = [...document.getElementsByClassName("media thread-body")];
    if (messages) {
      messages.forEach((element) => {
        const msgContent = element.getElementsByClassName(
          "message-body-content clearfix html"
        )[0];
        if (
          msgContent &&
          (msgContent.getElementsByClassName("ts-image").length != 0 ||
            msgContent.getElementsByClassName("inline-compose-card").length !=
              0)
        ) {
          if (msgContent.innerText === text) {
            replaceMessageContent(msgContent);
          }
        }
      });
    }
  }, 1000);
};

const updateMessages = function updateMessages() {
  if (document.getElementsByClassName("message-pane")[0]) {
    updateMessagesChannel();
  } else {
    console.log("messages update");
    const iframeDocument = document.getElementsByClassName(
      "embedded-electron-webview embedded-page-content"
    )[0].contentWindow.document;
    const messages = [
      ...iframeDocument.getElementsByClassName("ui-chat__item"),
    ];

    if (messages) {
      messages.forEach((element) => {
        if (element) {
          if (
            element.getElementsByClassName("ui-image").length === 0 &&
            element.getElementsByClassName("ui-grid").length === 0
          ) {
            replaceMessageContent(element);
          } else {
            if (
              (!element.innerText.includes("Loading\n") &&
                !element.innerText.includes("Loading image...\n") &&
                element.innerText !== "Loading" &&
                element.innerText !== "Loading image...") ||
              element.getElementsByClassName("ui-loader").length != 0
            ) {
              findMessageAndUpdate(element.innerText);
            }
          }

          const msgAuthorName = element.getElementsByClassName(
            "ui-chat__message__author"
          )[0];
          if (msgAuthorName) replaceName(msgAuthorName);
          const msgAuthorImg = element.getElementsByTagName("img")[0];
          if (msgAuthorImg) replaceImg(msgAuthorImg);
        } else {
          console.log("no message content");
        }
      });
    }

    const chatHeader = document.getElementsByClassName(
      "app-chat-header-content"
    )[0];

    if (chatHeader) {
      const chatHeaderName = chatHeader.getElementsByClassName(
        "app-participant-unit"
      )[0];
      if (chatHeaderName) {
        replaceName(chatHeaderName);
        const chatHeaderImg = chatHeader.getElementsByTagName("img")[0];
        replaceImg(chatHeaderImg);
      }
    }
  }
};

const updateMessagesChannel = function updateMessagesChannel() {
  console.log("messages update");
  const messages = [...document.getElementsByClassName("media thread-body")];

  if (messages) {
    messages.forEach((element) => {
      const msgContent = element.getElementsByClassName(
        "message-body-content clearfix html"
      )[0];
      if (msgContent) {
        if (
          msgContent.getElementsByClassName("ts-image").length === 0 &&
          msgContent.getElementsByClassName("inline-compose-card").length === 0
        ) {
          replaceMessageContent(msgContent);
        } else {
          if (
            !msgContent.innerText.includes("Loading\n") &&
            !msgContent.innerText.includes("Loading image...\n") &&
            msgContent.innerText !== "Loading" &&
            msgContent.innerText !== "Loading image..."
          ) {
            findMessageAndUpdateChannel(msgContent.innerText);
          }
        }

        const msgAuthorName = element.getElementsByClassName("ts-msg-name")[0];
        replaceName(msgAuthorName);
        const msgAuthorImg = element.getElementsByTagName("img")[0];
        replaceImg(msgAuthorImg);
      }
    });
  }

  const chatHeader = document.getElementsByClassName(
    "app-chat-header-content"
  )[0];

  if (chatHeader) {
    const chatHeaderName = chatHeader.getElementsByClassName(
      "app-participant-unit"
    )[0];
    if (chatHeaderName) {
      replaceName(chatHeaderName);
      const chatHeaderImg = chatHeader.getElementsByTagName("img")[0];
      replaceImg(chatHeaderImg);
    }
  }
};

const updateChats = function updateChats() {
  console.log("chat update");
  const chats = [...document.getElementsByClassName("ui-chat__item")];

  if (chats) {
    chats.forEach(replaceChatContent);
    const names = [
      ...document.getElementsByClassName(
        "cle-title single-line-truncation truncate-name cle-margin"
      ),
    ];

    names.forEach((element) => {
      replaceName(element);
      element.setAttribute(
        "style",
        "direction: rtl; margin-inline-start: auto;"
      );
    });

    const chatProfilePics = [
      ...document.getElementsByClassName("profile-img-parent"),
    ];

    chatProfilePics.forEach((element) => {
      const chatProfileImg = element.getElementsByTagName("img")[0];
      replaceImg(chatProfileImg);
    });
  }
};

const updateCard = function updateCard() {
  const cardContainer = document.getElementById("LivePersonaCardRootElement");
  const card = cardContainer.querySelector(
    "[data-log-region='LivePersonaCard']"
  );

  if (card) {
    console.log("card update");
    const name = card.querySelector('[data-log-name="DisplayName"]');

    if (name) {
      replaceName(name);
      name.setAttribute("style", "direction: rtl");
    }

    const img = card.getElementsByTagName("img")[0];
    img.alt = img.getAttribute("src");
    replaceImg(img);
    const imgLink = document.createElement("a");
    imgLink.setAttribute("href", img.getAttribute("src"));
    imgLink.setAttribute("target", "_blank");
    const parent = img.parentNode;
    imgLink.innerHTML = parent.innerHTML;
    parent.replaceChild(imgLink, img);
  } else {
    console.log("no card");
  }
};

const reloadDB = async () => {
  ({
    words: wordDictionary,
    names: nameDictionary,
    images: imgDictionary,
  } = await fetchDataNoCache());
};

export { updateMessages, updateChats, updateCard, reloadDB };
