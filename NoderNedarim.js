// Popup.js code

import { getMainDB, updateDB, getSuggestions, sendSuggestions } from "./api.js";
import { confetti, killConfetti } from "./confetti.js";

document.getElementById("buttonText").innerHTML = "נודר";
document.getElementById("findText").innerHTML = document
  .getElementById("findText")
  .innerHTML.replace("findLabel", "שזה");

document.getElementById("replaceText").innerHTML = document
  .getElementById("replaceText")
  .innerHTML.replace("replaceLabel", "יהיה זה");

const clicked = async function clicked() {
  const find = document.getElementById("find");
  const replace = document.getElementById("replace");
  const { value: newKey } = find;
  const { value: newValue } = replace;

  if (newKey && newValue) {
    try {
      const { content: db, sha: commitID } = await getSuggestions();
      const existingValues = db[newKey];
      const values = existingValues ? [...existingValues, newValue] : [newValue];
      db[newKey] = values;
      const update = { db, commitID, message: `${newKey} to ${newValue}` };
      await sendSuggestions(update);
      find.value = "";
      replace.value = "";
      sendNotification("success");
      launchConfetti();
    } catch (error) {
      sendNotification("it's fucked");
    }
  }
};

const sendNotification = function sendNotification(result) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { result: result }, function (response) {
      console.log(response.farewell);
    });
  });
};

let isEilay = false;
const dictionaries = ["words", "names", "images"];

const eliayClick = async function eilayClick() {
  const find = document.getElementById("find");
  const replace = document.getElementById("replace");
  const { value: newKey } = find;
  const { value: newValue } = replace;

  if (newKey && newValue) {
    try {
      const type = dictionaries[0];
      const { content: db, sha: commitID } = await getMainDB("main");
      const existingValues = db[type][newKey];
      const values = existingValues ? [...existingValues, newValue] : [newValue];
      db[type][newKey] = values;
      const update = { db, commitID, message: `${newKey} to ${newValue}` };
      await updateDB(update);
      find.value = "";
      replace.value = "";
      sendNotification("success");
      sendNotification("reloadDB");
      launchConfetti();
    } catch (error) {
      sendNotification("it's fucked");
    }
  }
};

const editPermission = function editPermmision() {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { result: "salt" }, function (response) {
      if (response.isAllowed === "true") {
        isEilay = true;
        sendNotification(dictionaries[0]);
      } else {
        wasPressedBefore = false;
        sendNotification("reloadDB");
      }
    });
  });
};

const launchConfetti = function launchConfetti() {
  setTimeout(() => confetti("body"), 200);
  setTimeout(killConfetti, 3700);
};

const callback = () => (isEilay ? eliayClick() : clicked());

const button = document.getElementById("button");
button.addEventListener("click", callback);

let wasPressedBefore = false;

const salt = function salt() {
  if (wasPressedBefore) {
    dictionaries.push(dictionaries.shift());
    sendNotification(dictionaries[0]);
  } else {
    editPermission();
  }

  wasPressedBefore = true;
};

const saltIcon = document.getElementById("salt");
saltIcon.addEventListener("click", salt);
